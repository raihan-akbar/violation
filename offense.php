<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="icon" type="image/png" href="<?php echo base_url('img/favicon.png')?>"/>
        
        <title>Offense</title>

        <?php include '_source/zircos_head.php'; ?>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php include '_partials/navbar.php'; ?>

            <?php include '_partials/sidebar.php'; ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Dashboard </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                       <li>Violation</li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <!-- Batas Atas Mulai Disini -->
                        

                                    <!-- Batas Bawah -->
                                     
                        </div>
                    </div>
                    <!-- Batas -->



                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    Punten Slur &copy; <?php echo date('Y') ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <?php include '_source/zircos_foot.php'; ?>



    </body>
  </html>

