<?php 
class Mc extends CI_Model{

	function check($table,$where){
		return $this->db->get_where($table,$where);
	}
	function add($data,$table){
		$this->db->insert($table,$data);
	}
	function del($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function upd($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	
	function menu()
	{
		return $this->db->query("SELECT * FROM sidebar ORDER BY sequence ASC ");
	}

	function menuUser()	
	{
		return $this->db->query("SELECT * FROM sidebar WHERE access='All' ORDER BY sequence ASC ");
	}

	function student()
	{
		return $this->db->query("SELECT * FROM student,expertise WHERE expertise.id_expertise=student.id_expertise ORDER BY name ASC");
	}

	function offense()
	{
		return $this->db->query("SELECT * FROM offense");
	}

	function expertise()
	{
		return $this->db->query("SELECT * FROM expertise");
	}

	function getAccount()
	{
		$this->db->select("*");
		$this->db->from('user');
		$this->db->where('username',$this->session->userdata('username'));
		return $this->db->get();
	}


	function getRoleAccount()
	{
		$un = $this->session->userdata('username');
		return $this->db->query("SELECT * FROM user,role WHERE role.id_role=user.id_role AND user.username='$un' ");
	}

	function getRecordData()
	{
		
		//return $this->db->query("SELECT * FROM record ");
		return $this->db->query("SELECT * FROM student,record,offense,expertise,user WHERE student.id_student=record.id_student AND offense.id_offense=record.id_offense AND student.id_expertise=expertise.id_expertise AND record.id_user=user.id_user ORDER BY date_input DESC");
	}

	function catchSt()
	{
		$id_student = $this->session->userdata('id_student');
		return $this->db->query("SELECT * FROM student,expertise WHERE id_student='$id_student' AND expertise.id_expertise=student.id_expertise ");
	}

	function vioLast()
	{
		return $this->db->query("SELECT * FROM record,student,offense WHERE student.id_student=record.id_student AND offense.id_offense=record.id_offense ORDER BY date_input DESC LIMIT 1");
	}

	function vioCount(){
		return $this->db->query("SELECT COUNT(*) as jumlah from record");
	}

	function vioHiPerson()
	{  
     
		return $this->db->query("SELECT * FROM record,student,offense WHERE student.id_student=record.id_student AND offense.id_offense=record.id_offense ORDER BY point DESC LIMIT 1");
	}

	function vioPointPerson()
	{  
     	
     	$id_student = $this->session->userdata('id_student');
		return $this->db->query("SELECT SUM(cost) as point_total FROM record WHERE id_student='$id_student'");
	}

	function vioThisMonth(){

		date_default_timezone_set("Asia/Jakarta");

        $mon = date('F');
        $yea = date('Y');
        //$dit = date('d F, ');echo date(' l, ');echo date('h:i:sa');

		return $this->db->query("SELECT COUNT(*) as jumlah from record");
	}

	
	}//End of File qui