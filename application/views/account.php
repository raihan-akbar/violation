<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="icon" type="image/png" href="<?php echo base_url('img/favicon.png')?>"/>
        
        <title>Account</title>

        <?php include '_source/zircos_head.php'; ?>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php include '_partials/navbar.php'; ?>
            <?php include '_partials/sidebar.php'; ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Account Details </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                       <li><i class="fa fa-user"></i></li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <?php 
                        foreach ($roleAccounts as $ra) {
                        }
                         ?>

                        <?php
                         foreach ($accounts as $a) {
                             ?>

                        <?php 
                        $since = $a->since;
                        $sub_since = substr($since, 0,-8);
                        $uname = $a->username;
                         ?>

                        <!-- Batas Atas Mulai Disini -->

                        <?php echo $this->session->flashdata('cps');?>
                        <?php echo $this->session->flashdata('cpw');?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-4">
                                            <div class="text-center card-box">
                                                <div class="member-card">
                                                    <div class="thumb-xl member-thumb m-b-10 center-block">
                                                        <img style="width: 120px; height: 120px" src="<?php echo base_url('upload/avatar/default.png') ?>" class="img-circle img-thumbnail" alt="profile-image">
                                                    </div>

                                                    <div class="">
                                                        <a href="" class="m-b-5" data-toggle="modal" data-target="#add-modal">Change <i class="fa fa-pencil"></i></a>
                                                    </div>

                                                    <div class="">
                                                        <h4 class="m-b-5"><?php echo $a->s_name; ?></h4>
                                                    </div>

                                                    <hr/>

                                                    <div class="text-left">

                                                        
                                                    <a href="" data-toggle="modal" data-target="#edit-modal" class="">Edit Account Information</a>
                                                    <hr/>
                                                    <a href="" data-toggle="modal" data-target="#password-modal" class="">Change Password</a>

                                                        
                                                    </div>

                                                </div>

                                            </div> <!-- end card-box -->

                              
                                        </div> <!-- end col -->


                                        <div class="col-md-8 col-lg-9">

                                            <div class="row">
                                                <div class="col-md-8 col-sm-6">

                                                    <p>Account Information</p>

                                                        <p class="text-muted font-13"><strong>Name :</strong><span class="m-l-15"><?php echo $a->s_name; ?></span></p>

                                                         <p class="text-muted font-13"><strong>Username :</strong><span class="m-l-15"><?php echo $a->username; ?></span></p>

                                                    <p class="text-muted font-13"><strong>Session Role :</strong><span class="m-l-15"><?php echo $ra->name_role; ?></span></p>

                                                        <p class="text-muted font-13"><strong>Register :</strong> <span class="m-l-15"><?php echo $sub_since; ?></span></p>

                                                </div> <!-- end col -->
                                            </div> <!-- end row -->
                                            <hr/>
                                                

                                            </div> <!-- end row -->

                                            <hr/>

                                            
                                            </div>
                                        </div>
                                        <!-- end col -->

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                    <?php } ?>

                                    <!-- Batas Bawah -->
                                     
                        </div>
                    </div>
                    <!-- Batas -->



                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    Punten Slur &copy; <?php echo date('Y') ?>
                    
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <?php include '_source/zircos_foot.php'; ?>



    </body>

    <!-- Modal Avatar -->
 <?php echo form_open_multipart('Master/avaUpload'); ?>
    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Profile Picture +</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">

                                                            <div class="form-group text-center">
                                                                <img class="" style="width: 120px" src="<?php echo base_url('upload/avatar/default.png') ?>"><br><br>
                                                                <a href="" style="color: red">Remove Profile Picture</a>
                                                                
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Upload Image</label>
                                                                <input type="file" name="avatar" class="form-control">
                                                            </div>

                                                            <div class="form-group">
                                                                <p>For a good size: <code>1:1 / Square</code> or <code>200px x 200px</code></p>

                                                            </div>

                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="adds" type="submit" class="btn btn-facebook waves-effect waves-light">Save +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
 <?php echo form_close(); ?>
        <!-- Modal Avatar -->

        <!-- Modal Edit -->
<form action="<?php echo base_url(). 'Master/addStudent' ?>" method="post">
    <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Edit Information +</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Upload Image</label>
                                                                <input type="text" id="filter_input2" name="avatar" class="form-control">

                                                            </div>

                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Save +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Edit -->

        <!-- Modal Password -->
<form action="<?php echo base_url(). 'Master/updPassword' ?>" method="post">
    <div id="password-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

        <input type="hidden" name="username" value="<?php echo $a->username ?>">
        <input type="hidden" name="id_user" value="<?php echo $a->id_user ?>">

                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Change Password</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">

                                                            <div class="form-group">
                                                                <p>Old Password</p>
                                                                <input type="password" name="pwd" class="form-control" placeholder="Type Here">

                                                            </div>

                                                            <div class="form-group">
                                                                <p>New Password</p>
                                                                <input type="password" name="passwd" class="form-control" placeholder="Type Here" id="passwd">
                                                                
                                                            </div>

                                                            <div class="form-group">
                                                                <p>Re-Type Password</p>
                                                                <input type="password" name="password2" class="form-control" placeholder="Type Here" id="passwd2">
                                                                <p><strong></strong></p>
                                                                
                                                            </div>

                                                            <div class="form-group">
                                                                <p>Good Combination <code>Numeric</code> and <code>Uppercase + Lowercase</code></p>

                                                            </div>

                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="submit-password" type="submit" class="btn btn-facebook waves-effect waves-light" data-dismiss="modal">Save +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Password -->
  </html>

<script type="text/javascript">
        $(function () {
            $("#submit-password").click(function () {
                var pasa = $("#passwd").val();
                var padu = $("#passwd2").val();
                if (pasa != padu) {
Command: toastr["error"]("New Password Do not Match!", "Failed")

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1500",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

                    return false;
                }
                return true;
            });
        });
    </script>
