<!DOCTYPE html>
<html>
<head>
    <title>Nothing Here</title>
</head>
<?php include '_source/style_zircos.php'; ?>
<body class="bg-dark">

        <!-- HOME -->
        <section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-12 text-center">

                        <div class="wrapper-page">
                            <img src="<?php echo base_url('landing/images/charger.gif')?>" alt="" height="120">
                            <h2 class="text-uppercase text-danger">Page Not Found</h2>
                            <p class="text-muted">It's looking like you may have taken a wrong turn. Don't worry... it
                                happens to the best of us. You might want to check your internet connection. Here's a
                                little tip that might help you get back on track.</p>

                            <a class="btn btn-info waves-effect waves-light m-t-20" href="/violation/Master/"> Return Home</a>
                        </div>

                    </div>
                </div>
            </div>
          </section>

</body>
</html>