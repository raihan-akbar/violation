<!-- jQuery -->
<script src="<?php echo base_url('landing/plugins/jQuery/jquery.min.js')?>"></script>
<!-- Bootstrap JS -->
<script src="<?php echo base_url('landing/plugins/bootstrap/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/aos/aos.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/shuffle/shuffle.min.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/magnific-popup/jquery.magnific-popup.min.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/date-picker/datepicker.min.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/clock-picker/clockpicker.min.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/video-popup/jquery-modal-video.min.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/swiper/swiper.min.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/instafeed/instafeed.min.js')?>"></script>
<script src="<?php echo base_url('landing/plugins/bootstrap-touchpin/jquery.bootstrap-touchspin.min.js')?>"></script>

 
<!-- Main Script -->
<script src="<?php echo base_url('landing/js/contact.js')?>"></script>
<script src="<?php echo base_url('landing/js/script.js')?>"></script>