<!-- jQuery  -->
        <script src="<?php echo base_url('admin/assets/js/jquery.min.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/detect.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/fastclick.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/jquery.blockUI.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/waves.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/jquery.slimscroll.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/jquery.scrollTo.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/switchery/switchery.min.js')?>"></script>

        <!-- Counter js  -->
        <script src="<?php echo base_url('admin/plugins/waypoints/jquery.waypoints.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/counterup/jquery.counterup.min.js')?>"></script>

        <!--Morris Chart-->
                <script src="<?php echo base_url('admin/plugins/morris/morris.min.js')?>"></script>
                <script src="<?php echo base_url('admin/plugins/raphael/raphael-min.js')?>"></script>
                <script src="<?php echo base_url('admin/assets/pages/jquery.morris.init.js')?>"></script>

        <!-- Dashboard init -->
        <script src="<?php echo base_url('admin/assets/pages/jquery.dashboard.js')?>"></script>

        <!-- App js -->
        <script src="<?php echo base_url('admin/assets/js/jquery.core.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/jquery.app.js')?>"></script>

        <!-- Modal-Effect -->
        <script src="<?php echo base_url('admin/plugins/custombox/js/custombox.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/custombox/js/legacy.min.js')?>"></script>

        <!-- responsive-table-->
        <script src="<?php echo base_url('admin/plugins/responsive-table/js/rwd-table.min.js')?>" type="text/javascript"></script>


    <!-- DAta Table-->
        <script src="<?php echo base_url('admin/plugins/datatables/jquery.dataTables.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/dataTables.bootstrap.js')?>"></script>

        <script src="<?php echo base_url('admin/plugins/datatables/dataTables.buttons.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/buttons.bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/jszip.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/pdfmake.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/vfs_fonts.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/buttons.html5.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/buttons.print.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/dataTables.fixedHeader.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/dataTables.keyTable.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/dataTables.responsive.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/responsive.bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/dataTables.scroller.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/dataTables.colVis.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/datatables/dataTables.fixedColumns.min.js')?>"></script>

        <!-- Sweet-Alert  -->
        <script src="<?php echo base_url('admin/plugins/bootstrap-sweetalert/sweet-alert.min.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/pages/jquery.sweet-alert.init.js')?>"></script>

<!-- init -->
        <script src="<?php echo base_url('admin/assets/pages/jquery.datatables.init.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/pages/jquery.form-pickers.init.js')?>"></script>
<!-- Toastr js -->
        <script src="<?php echo base_url('admin/plugins/toastr/toastr.min.js')?>"></script>
        <!-- Toastr init js (Demo)-->
        <script src="<?php echo base_url('admin/assets/pages/jquery.toastr.js')?>"></script>


        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                $('#datatable-keytable').DataTable({keys: true});
                $('#datatable-responsive').DataTable();
                $('#datatable-colvid').DataTable({
                    "dom": 'C<"clear">lfrtip',
                    "colVis": {
                        "buttonText": "Hi"
                    }
                });
                $('#datatable-scroller').DataTable({
                    ajax: "<?php echo base_url('admin/plugins/datatables/json/scroller-demo.json')?>",
                    deferRender: true,
                    scrollY: 380,
                    scrollCollapse: true,
                    scroller: true
                });
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
                var table = $('#datatable-fixed-col').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: false,
                    fixedColumns: {
                        leftColumns: 1,
                        rightColumns: 1
                    }
                });
            });
            TableManageButtons.init();

        </script>

<!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="../plugins/jquery-knob/excanvas.js"></script>
        
        <![endif]-->
        
        <script type="text/javascript" src="<?php echo base_url('admin/plugins/jquery-knob/excanvas.js')?>"></script>

        <!-- Jquery filer js -->
        <script src="<?php echo base_url('admin/plugins/jquery.filer/js/jquery.filer.min.js')?>"></script>

        <!-- page specific js -->
        <script src="<?php echo base_url('admin/assets/pages/jquery.fileuploads.init.js')?>"></script>


<!-- SelectPICKER -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap-select-1.13.9/dist/css/bootstrap-select.min.css')?>">
    <script src="<?php echo base_url('bootstrap-select-1.13.9/dist/js/bootstrap-select.min.js') ?>"></script>

<!-- Date & Time Picker -->
       <script src="<?php echo base_url('admin/plugins/moment/moment.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/timepicker/bootstrap-timepicker.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/clockpicker/js/bootstrap-clockpicker.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/bootstrap-daterangepicker/daterangepicker.js')?>"></script>