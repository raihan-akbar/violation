<!-- Mobile Specific Meta
    ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Favicon -->
    <!-- <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" /> -->
    
    <!-- CSS
    ================================================== -->
    <!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/themefisher-font/style.css')?>">
    <!-- bootstrap.min css -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/bootstrap/dist/css/bootstrap.min.css')?>">
    <!-- Animate.css -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/animate-css/animate.css')?>">
        <!-- Magnific popup css -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/magnific-popup/dist/magnific-popup.css')?>">
    <!-- Slick Carousel -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/slick-carousel/slick/slick.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/slick-carousel/slick/slick-theme.css')?>">
    <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url('portal/css/style.css')?>">

<!-- CODE ATAS - END -->

<!-- Code Bawah - Start -->

<!-- 
    Essential Scripts
    =====================================-->
    
    <!-- Main jQuery -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap 3.1 -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <!-- Slick Carousel -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/slick-carousel/slick/slick.min.js')?>"></script>
    <!-- Portfolio Filtering -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/mixitup/dist/mixitup.min.js')?>"></script>
    <!-- Smooth Scroll -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/smooth-scroll/dist/js/smooth-scroll.min.js')?>"></script>
    <!-- Magnific popup -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/magnific-popup/dist/jquery.magnific-popup.min.js')?>"></script>
    <!-- Google Map API -->

    <!--<script type="text/javascript"  src="http://maps.google.com/maps/api/js?sensor=false"></script>-->

    <!-- Sticky Nav -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/Sticky/jquery.sticky.js')?>"></script>
    <!-- Number Counter Script -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/count-to/jquery.countTo.js')?>"></script>
    <!-- wow.min Script -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/wow/dist/wow.min.js')?>"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url('portal/js/script.js')?>"></script>