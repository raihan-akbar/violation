<!--Morris Chart CSS -->
        <link rel="stylesheet" href="<?php echo base_url('admin/plugins/morris/morris.css')?>">

        <!-- App css -->
        <link href="<?php echo base_url('admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/core.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/components.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/icons.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/pages.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/menu.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/responsive.css" rel="stylesheet" type="text/css')?>" />
        <link rel="stylesheet" href="<?php echo base_url('admin/plugins/switchery/switchery.min.css')?>">


        <script src="<?php echo base_url('admin/assets/js/modernizr.min.js')?>"></script>

<!-- Batas -->

<!-- jQuery  -->
        <script src="<?php echo base_url('admin/assets/js/jquery.min.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/detect.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/fastclick.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/jquery.blockUI.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/waves.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/jquery.slimscroll.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/jquery.scrollTo.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/switchery/switchery.min.js')?>"></script>

        <!-- Counter js  -->
        <script src="<?php echo base_url('admin/plugins/waypoints/jquery.waypoints.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/counterup/jquery.counterup.min.js')?>"></script>

        <!--Morris Chart-->
                <script src="<?php echo base_url('admin/plugins/morris/morris.min.js')?>"></script>
                <script src="<?php echo base_url('admin/plugins/raphael/raphael-min.js')?>"></script>

        <!-- Dashboard init -->
        <script src="<?php echo base_url('admin/assets/pages/jquery.dashboard.js')?>"></script>

        <!-- App js -->
        <script src="<?php echo base_url('admin/assets/js/jquery.core.js')?>"></script>
        <script src="<?php echo base_url('admin/assets/js/jquery.app.js')?>"></script>

        <!-- Modal-Effect -->
        <script src="<?php echo base_url('admin/plugins/custombox/js/custombox.min.js')?>"></script>
        <script src="<?php echo base_url('admin/plugins/custombox/js/legacy.min.js')?>"></script>