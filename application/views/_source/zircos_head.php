<!-- Table Responsive css -->
        <link href="<?php echo base_url('admin/plugins/responsive-table/css/rwd-table.min.css')?>" rel="stylesheet" type="text/css" media="screen">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="<?php echo base_url('admin/plugins/morris/morris.css')?>">

        <!-- App css -->
        <link href="<?php echo base_url('admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/core.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/components.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/icons.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/pages.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/menu.css" rel="stylesheet" type="text/css')?>" />
        <link href="<?php echo base_url('admin/assets/css/responsive.css" rel="stylesheet" type="text/css')?>" />
        <link rel="stylesheet" href="<?php echo base_url('admin/plugins/switchery/switchery.min.css')?>">


        <script src="<?php echo base_url('admin/assets/js/modernizr.min.js')?>"></script>

        <!-- DataTables -->
        <link href="<?php echo base_url('admin/plugins/datatables/jquery.dataTables.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('admin/plugins/datatables/buttons.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('admin/plugins/datatables/fixedHeader.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('admin/plugins/datatables/responsive.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('admin/plugins/datatables/scroller.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('admin/plugins/datatables/dataTables.colVis.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('admin/plugins/datatables/dataTables.bootstrap.min.css')?>" rel="stylesheet" type="text/css')?>"/>
        <link href="<?php echo base_url('admin/plugins/datatables/fixedColumns.dataTables.min.css')?>" rel="stylesheet" type="text/css')?>"/>

        <!-- Sweet Alert -->
        <link href="<?php echo base_url('admin/plugins/bootstrap-sweetalert/sweet-alert.css')?>" rel="stylesheet" type="text/css">

        <!-- Notification css (Toastr) -->
        <link href="<?php echo base_url('admin/plugins/toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />

        <!-- Jquery filer css -->
        <link href="<?php echo base_url('admin/plugins/jquery.filer/css/jquery.filer.css')?>" rel="stylesheet" />
        <link href="<?php echo base_url('admin/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css')?>" rel="stylesheet" />

        <!-- Date & Time Picker -->
        <link href="<?php echo base_url('admin/plugins/timepicker/bootstrap-timepicker.min.css')?>" rel="stylesheet">
                <link href="<?php echo base_url('admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')?>" rel="stylesheet">
                <link href="<?php echo base_url('admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')?>" rel="stylesheet">
                <link href="<?php echo base_url('admin/plugins/clockpicker/css/bootstrap-clockpicker.min.css')?>" rel="stylesheet">
                <link href="<?php echo base_url('admin/plugins/bootstrap-daterangepicker/daterangepicker.css')?>" rel="stylesheet">
