

<!-- Code Bawah - Start -->

<!-- 
    Essential Scripts
    =====================================-->
    
    <!-- Main jQuery -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap 3.1 -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <!-- Slick Carousel -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/slick-carousel/slick/slick.min.js')?>"></script>
    <!-- Portfolio Filtering -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/mixitup/dist/mixitup.min.js')?>"></script>
    <!-- Smooth Scroll -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/smooth-scroll/dist/js/smooth-scroll.min.js')?>"></script>
    <!-- Magnific popup -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/magnific-popup/dist/jquery.magnific-popup.min.js')?>"></script>
    <!-- Google Map API -->

    <!--<script type="text/javascript"  src="http://maps.google.com/maps/api/js?sensor=false"></script>-->

    <!-- Sticky Nav -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/Sticky/jquery.sticky.js')?>"></script>
    <!-- Number Counter Script -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/count-to/jquery.countTo.js')?>"></script>
    <!-- wow.min Script -->
    <script type="text/javascript" src="<?php echo base_url('portal/plugins/wow/dist/wow.min.js')?>"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url('portal/js/script.js')?>"></script>