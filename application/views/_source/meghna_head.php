<!-- Mobile Specific Meta
    ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Favicon -->
    <!-- <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" /> -->
    
    <!-- CSS
    ================================================== -->
    <!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/themefisher-font/style.css')?>">
    <!-- bootstrap.min css -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/bootstrap/dist/css/bootstrap.min.css')?>">
    <!-- Animate.css -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/animate-css/animate.css')?>">
        <!-- Magnific popup css -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/magnific-popup/dist/magnific-popup.css')?>">
    <!-- Slick Carousel -->
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/slick-carousel/slick/slick.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('portal/plugins/slick-carousel/slick/slick-theme.css')?>">
    <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url('portal/css/style.css')?>">

<!-- CODE ATAS - END -->