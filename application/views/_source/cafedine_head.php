<!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/bootstrap/bootstrap.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/themify/css/themify-icons.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/icofont/icofont.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/fontawesome/css/all.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/aos/aos.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/magnific-popup/magnific-popup.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/video-popup/modal-video.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/swiper/swiper.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/date-picker/datepicker.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/clock-picker/clockpicker.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/bootstrap-touchpin/jquery.bootstrap-touchspin.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('landing/plugins/devices.min.css')?>">

  <!-- Main Stylesheet -->
  <link href="<?php echo base_url('landing/css/style.css')?>" rel="stylesheet">

<!-- Custom Font -->
<link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">