F<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="icon" type="image/png" href="<?php echo base_url('img/favicon.png')?>"/>
        
        <title>Dashboard</title>

        <?php include '_source/zircos_head.php'; ?>

        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">
            <?php include '_partials/navbar.php'; ?>

            <?php include '_partials/sidebar.php'; ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Student Details </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                       <li><i class="fa fa-newspaper-o"></i></li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <?php
                         foreach ($cStudent as $c) {
                             ?>

                        <?php 
                        $since = $c->nis;
                        $sub_since = substr($since, 0,-8);
                        $uname = $c->name;
                        $id_st= $c->id_student;
                         ?>

                         <?php
                         foreach ($vioPointPersons as $vpp) {

                             ?>
                             <?php 

                             $total_point=$vpp->point_total

                         
                              ?>

                        <?php 
                        $since = $c->nis;
                        $sub_since = substr($since, 0,-8);
                        $uname = $c->name;
                    }
                         ?>


                        <!-- Batas Atas Mulai Disini -->

                        <?php echo $this->session->flashdata('cps');?>
                        <?php echo $this->session->flashdata('cpw');?>



                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-4">
                                            <div class="text-center card-box">
                                                <div class="member-card">
                                                    <div class="thumb-xl member-thumb m-b-10 center-block">
                                                        <img style="width: 120px; height: 120px" src="<?php echo base_url('upload/avatar/default.png') ?>" class="img-circle img-thumbnail" alt="profile-image">
                                                    </div>

                                                    <div class="">
                                                        <a href="" class="m-b-5" data-toggle="modal" data-target="#add-modal">Change <i class="fa fa-pencil"></i></a>
                                                    </div>

                                                    <div class="">
                                                        <h4 class="m-b-5"><?php echo $c->name; ?></h4>
                                                    </div>

                                                    <hr/>

                                                    <div class="text-left">
                                                    <a style="color: green" class="" href=""class="" href="" data-toggle="modal" data-target="#add-achievement"><i class="fa fa-star"> </i> Add Achievement Record</a><hr>
                                                    <a style="color: red;" class="" href="" data-toggle="modal" data-target="#add-violation"><i class="fa fa-flag"></i> Add Violation Record</a>
                                                    <hr>
                                                    <hr/>
                                                    <a href=""  data-toggle="modal" data-target="#modal-view<?php echo $id_st ?>">Edit Student Information</a>
                                                    <hr/>

                                                        
                                                    </div>

                                                </div>

                                            </div> <!-- end card-box -->

                              
                                        </div> <!-- end col -->


                                        <div class="col-md-8 col-lg-9">

                                            <div class="row">
                                                <div class="col-md-12 col-sm-6">
                                                    <div class="text-right">
                                                    
                                                    </div>
                                                    <hr/>

                                                        <h4>NIS : <?php echo $c->nis; ?></h4>

                                                        <p class="text font-13"><strong>Nama :</strong>
                                                        <span class="m-l-15"><?php echo $c->name; ?></span></p>

                                                        <p class="text font-13"><strong>Expertise :</strong>
                                                        <span class="m-l-15"><?php echo $c->name_expertise; ?></span><span> (<?php echo $c->alias_expertise; ?>)</span></p>

                                                        <p class="text font-13"><strong>Start Periode :</strong>
                                                        <span class="m-l-15"><?php echo $c->start_period; ?></span></p>

                                                        <p class="text font-13"><strong>Address :</strong>
                                                        <span class="m-l-15"><?php echo $c->address; ?></span></p>
                                                        <p><?php echo $total_point ?></p>

                                                        <hr/>

                                <div class="col-lg-4 col-md-6">
                                <div class="card-box widget-box-two widget-two-teal">
                                    <i class="fa fa-star widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">TOTAL ACHIEVEMENT</p>
                                        <h2><span data-plugin="counterup"><?php echo $total_point; ?></span> <small><i class="fa fa-thumbs-up"></i></small></h2>
                                        <p class="text-muted m-0"><i class="fa fa-star"></i> Achievement</p>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            

                            <div class="col-lg-4 col-md-6">
                                <div class="card-box widget-box-two widget-two-purple">
                                    <i class="fa fa-cubes widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">TOTAL VIOLATION</p>
                                        <h2><span data-plugin="counterup"><?php echo $total_point; ?></span> <small><i class="fa fa-arrow-up"></i></small></h2>
                                        <p class="text-muted m-0"><i class="fa fa-circle"></i> Point</p>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-4 col-md-6">
                                <div class="card-box widget-box-two widget-two-inverse">
                                    <i class="fa fa-adjust widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">TOTAL POINT</p>
                                        <h2><span data-plugin="counterup"><?php echo $total_point; ?></span> <small><i class="fa fa-arrow-up"></i></small></h2>
                                        <p class="text-muted m-0"><i class="fa fa-circle"></i> Point</p>
                                    </div>
                                </div>
                            </div><!-- end col -->




                                                </div> <!-- end col -->
                                            </div> <!-- end row -->
                                            
                                                

                                            </div> <!-- end row -->

                                            <hr/>

                                            
                                            </div>
                                        </div>
                                        <!-- end col -->

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                    <?php } ?>

                                    <!-- Batas Bawah -->
                                     
                        </div>
                    </div>
                    <!-- Batas -->



                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    Punten Slur &copy; <?php echo date('Y') ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>
        <?php include '_source/zircos_foot.php'; ?>


    </body>

    <!-- Modal View -->

                                            <?php
                                            foreach ($students as $s) {
                                                
                                             ?>
    <form action="<?php echo base_url(). 'Master/updStudent' ?>" method="post">
        <div id="modal-view<?php echo $s->id_student ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add Achievement Record +</h4>
                                                    <input type="hidden" name="id_student" value="<?php echo $s->id_student ?>">
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">NIS</label>
                                                                <input type="text" id="field-3" name="nis" class="form-control" placeholder="17180xxxx" value="<?php echo $s->nis ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Name</label>
                                                                <input type="text" id="field-3" name="name" class="form-control" placeholder="Full Name" value="<?php echo $s->name ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Expertise</label>
                                                                <select name="id_expertise" id="field-2" class="form-control" required="true">
                                                <option disabled="true" selected="selected">Expertise</option>
                                                <?php foreach ($expertises as $e) {
                                                $selected=($options == $e->id_expertise)? "selected" : "";
 
                                                 ?>


                                                <option  value="<?php echo $e->id_expertise ?>"><?php echo $e->alias_expertise; ?></option>
                                            <?php } ?>
                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Point</label>
                                                               <input type="text" id="field-3" name="point" class="form-control" placeholder="Point" value="<?php echo $s->point ?>"> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Address</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Student Home Address" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="address"><?php echo $s->address ?></textarea>
                                                            </div>
                                                        </div>

                                                        <div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Year Added <small></small></label>
                                                               <input type="text" id="field-3" name="start_period" class="form-control" placeholder="Point" value="<?php echo $s->start_period ?>">
                                                            </div>
                                                        </div><div class="col-md-6">
                                                            <div class="form-group">
                                                            <p></p>
                                                            <p><code>Year Added</code> For Student Periode Group</p>
                                                            </div> 
                                                        </div>
                                                    </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                                                    <button type="submit" class="btn btn-teal waves-effect waves-light">Save Changes <i class="fa fa-pencil"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                    </form>
                                <?php } ?>

                                <!-- Modal Add Vio -->
<form action="<?php echo base_url('Master/addRecord'). '#' ?>" method="post">
    <div id="add-violation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add Violation Record +</h4>
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Violation Name</label>
                                                                <div>
                
                                                                <select id="id-vio" name="id_offense" class="form-control selectpicker" data-live-search="true">
                                                                    <option disabled="disabled" selected="selected">Select</option>
                                                                    <?php foreach ($offenses as $of) {
                                                                     ?>
                                                                    <option value="<?php echo $of->id_offense; ?>"><?php echo $of->name_offense; ?></option>
                                                                <?php } ?>
                                                                </select>

                                                                
                                                                </div>

                                                            </div>
                                                        </div>

                                                       <div class="col-md-6">
                                                                <label class="control-label">Date - Month - Year</label>
                                                                <div class="form-group no-margin input-group">
                                                                    <!-- <input name="d_record" type="text" class="form-control" placeholder="mm-dd-yyyy" id="datepicker-autoclose" 
                                                                    value="<?= date('d-m-Y') ?>"> -->
                                                                    <input type="text" name="d_record" id="datepicker-autoclose" class="form-control" data-date-format='dd-mm-yy' value="<?= date('d-m-Y') ?>">
                                                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span> 
                                                                </div>
                                                        </div>

                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">About</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Explain About this Case / Penalty" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_record"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Point Added</label>
                                                                <input type="number" min="0" name="cost" id="cost" class="form-control" placeholder="Cost of Point Added">
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add Record+</button>
                                                    <input type="hidden" name="id_student" value="<?php echo $c->id_student ?>">
                                                    <input type="hidden" name="id_user" value=" <?php echo $ra->id_user ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

<script type="text/javascript">
         $(document).ready(function(){

         });
        </script>
                                </form>

                                <!-- Modal Add Ach -->
<form action="<?php echo base_url(). 'Master/addExpertise' ?>" method="post">
    <div id="add-achievement" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add New Achievement +</h4>
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Name</label>
                                                                <input type="text" id="field-3" name="name_expertise" class="form-control" placeholder="Name of this Expertise">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Alias</label>
                                                               <input type="text" id="field-3" name="alias_expertise" class="form-control" placeholder="Alias of this Expertise Name"> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">About</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Explain About this Expertise" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_expertise"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add +</button>
                                                    <input type="" name="" value="<?php echo $c->id_student ?>">
                                                    <input type="" name="" value=" <?php echo $ra->id_user ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->



                                </form>
        <!-- Modal Add -->

  </html>

  
