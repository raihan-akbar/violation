<!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <!--<a href="index.html" class="logo"><span>Zir<span>cos</span></span><i class="mdi mdi-layers"></i></a>-->

                    <!-- Image logo -->
                    <a href="index.html" class="logo">
                        <span>
                            <img src="<?php echo base_url('img/tag.png')?>" alt="" height="30">
                        </span>
                        <i>
                            <img src="<?php echo base_url('img/logo-white.png')?>" alt="" height="28">
                        </i>
                    </a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Navbar-left -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="mdi mdi-menu"></i>
                                </button>

                        </ul>   


                        <!-- Right(Notification) -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list"> 
                                </ul>
                            <li class="dropdown user-box">
                                <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                                    <img src="<?php echo base_url('img/avatar.png')?>" alt="user-img" class="img-circle user-img">
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                    <li>
                                    </li>
                                      <li>
                                        <a href="account"><i class="ti-settings m-r-5"></i>Setting</a>
                                        
                                    </li>  
                                    <li>
                                        <a href="/violation/Auth/signout"><i class="fa fa-sign-out m-r-5"></i>Sign out</a>
                                    </li>
                                    
                                </ul>
                            </li>

                        </ul> <!-- end navbar-right -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->

            <?php 

        date_default_timezone_set("Asia/Jakarta");

        $mon = date('F');
        $yea = date('Y');
        $dat = date('d');
        $ymd = date('Y/m/d');

             ?>