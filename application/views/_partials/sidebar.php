<!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                        	<li class="menu-title">Menus</li>


            <?php 
            foreach ($roleAccounts as $ra) {
                if ($ra->name_role != 'Master') {
                    $vabsy=$menuser;
                }else{
                       $vabsy=$menus;
                     }
            }
            ?>

                            <?php foreach ($vabsy as $m): ?>
                                <li>
                                <a href="<?php echo $m->url ?>" class="waves-effect">
                                <i class="<?php echo $m->icon ?>">
                                </i><span> <?php echo $m->title ?> </span></a>
                            </li>
                            <?php endforeach ?>

                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                    <div class="text-center help-box">
                        <p>Need Help?</p>
                        <a href="https://t.me/raihan8" target="_blank">Telegram</a>
                        <?php foreach ($roleAccounts as $ra) {
                         ?>
                         <!-- <p><?php echo $ra->name_role; ?></p> -->
                     <?php } ?>
                     <p>Raihan Akbar</p>
                    </div>

                   <!-- <div class="help-box">
                        <h5 class="text-muted m-t-0">Programmer</h5>
                        <p class="m-b-0"><span class="text-custom">Need Help?</span></p><br>
                        raihngl19@gmail.com
                    </div>

                -->

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

