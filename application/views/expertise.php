<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="icon" type="image/png" href="<?php echo base_url('img/favicon.png')?>"/>
        
        <title>Dashboard</title>

        <?php include '_source/zircos_head.php'; ?>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php include '_partials/navbar.php'; ?>

            <?php include '_partials/sidebar.php'; ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Expertise </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                       <li><i class="fa fa-code-fork"></i></li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                        <p><i class="fa fa-code-fork"></i> Search aja biar Enak</p>

                        <!-- Batas Atas Mulai Disini -->
                        <div class="row"><!-- Start row-->
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <div class="text-right">
                                    <a class="btn btn-teal"  data-toggle="modal" data-target="#add-modal">Add Offense +</a>
                                </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Tag</th>
                                            <th class="col-md-6">About</th>
                                            <th><i class="fa fa-gears"></i></th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                            <?php 
                                            $no=1;
                                            foreach ($expertises as $e) {
                                            ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $e->name_expertise ?></td>
                                            <td><?php echo $e->alias_expertise ?></td>
                                            <td><?php echo $e->about_expertise ?></td>
                                            <td>
                                                <a class="btn btn-facebook" data-toggle="modal" data-target="#modal-view<?php echo $e->id_expertise ?>">
                                                    <i class="fa fa-external-link"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- End row -->
                        

                                    <!-- Batas Bawah -->
                                     
                        </div>
                    </div>
                    <!-- Batas -->



                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    Punten Slur &copy; <?php echo date('Y') ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <?php include '_source/zircos_foot.php'; ?>



    </body>

    <!-- Modal Add -->
<form action="<?php echo base_url(). 'Master/addExpertise' ?>" method="post">
    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add New Student +</h4>
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Name</label>
                                                                <input type="text" id="field-3" name="name_expertise" class="form-control" placeholder="Name of this Expertise">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Alias</label>
                                                               <input type="text" id="field-3" name="alias_expertise" class="form-control" placeholder="Alias of this Expertise Name"> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">About</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Explain About this Expertise" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_expertise"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button id="add" type="submit" class="btn btn-facebook waves-effect waves-light">Add +</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>
        <!-- Modal Add -->

        <!-- Modal View -->

                                            <?php
                                            foreach ($expertises as $o) {
                                                
                                             ?>
    <form action="<?php echo base_url(). 'Master/updExpertise' ?>" method="post">
        <div id="modal-view<?php echo $o->id_expertise?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add New Student +</h4>
                                                    <input type="hidden" name="id_expertise" value="<?php echo $o->id_expertise ?>">
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Name</label>
                                                                <input type="text" id="field-3" name="name_expertise" class="form-control" placeholder="Name of this Expertise" value="<?php echo $o->name_expertise ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Alias</label>
                                                               <input type="text" id="field-3" name="alias_expertise" class="form-control" placeholder="Alias of this Expertise Name" value="<?php echo $o->alias_expertise ?>"> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">About</label>
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Explain About this Expertise" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;" name="about_expertise"> <?php echo $o->about_expertise ?></textarea>                                                        
                                                            </div>
                                                            <a style="color: red;" onclick="del()" data-dismiss="modal">Delete Data <i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                                                    <button type="submit" class="btn btn-teal waves-effect waves-light">Save Changes <i class="fa fa-pencil"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                    </form>
  
 <script>//Swal Delete Offense
     function del() {
         swal({
  title: "Delete Expertise Data?",
  text: "Your will not be able to recover this Expertise Data!",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Master/delExpertise/'.$o->id_expertise;?>";

});

     }
 </script>

 <?php echo $this->session->flashdata('notify') ?>

  <?php } ?>
  </html>


            