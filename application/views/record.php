<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="icon" type="image/png" href="<?php echo base_url('img/favicon.png')?>"/>
        
        <title>Record</title>

        <?php include '_source/zircos_head.php'; ?>
        
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <?php include '_partials/navbar.php'; ?>

            <?php include '_partials/sidebar.php'; ?>
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Recorded</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                       <li><i class="fa fa-database"></i></li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <!-- Batas Atas Mulai Disini -->
                        <div class="row"><!-- Start row-->
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <div class="text-right">
                                </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>NIS</th>
                                            <th>Violation</th>
                                            <th class="col-md-1">Expertise</th>
                                            <th>Date</th>
                                            <th class="col-md-1">Point Add</th>
                                            <th><i class="fa fa-gears"></i></th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                            <?php 
                                            $no=1;
                                            foreach ($recordDatas as $r) {
                                                $date_source = explode('-', $r->d_record);
                                                $array = array($date_source[1], $date_source[2], $date_source[0]); //D-M-Y
                                                $date_record = implode('-', $array);
                                                $date = implode(array($date_source[2]));
                                            ?>
                                        <tr>
                                            <td><?php echo $r->name ?></td>
                                            <td><?php echo $r->nis ?></td>
                                            <td><?php echo $r->name_offense ?></td>
                                            <td><?php echo $r->alias_expertise ?></td>
                                            <td><?php echo $date_record ?></td>
                                            <td><?php echo $r->cost ?></td>
                                            <td>
                                                <a class="btn btn-facebook" data-toggle="modal" data-target="#modal-view<?php echo $r->id_offense ?>">
                                                    <i class="fa fa-external-link"></i>
                                                    <a href=""></a>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- End row -->
                        

                                    <!-- Batas Bawah -->
                                     
                        </div>
                    </div>
                    <!-- Batas -->



                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    Punten Slur &copy; <?php echo date('Y') ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <?php include '_source/zircos_foot.php'; ?>



    </body>
  </html>

