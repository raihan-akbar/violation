<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="Meghna One page parallax responsive HTML Template ">
        
        <meta name="author" content="Muhammad Morshed">
        
        <title>Login</title>
        <link rel="icon" type="image/png" href="<?php echo base_url('img/favicon.png')?>"/>
                
        <?php include '_source/style_meghna.php'; ?>

    </head>
    
    <body id="body" data-spy="scroll" data-target=".navbar" data-offset="50">
        
<!-- Srart Contact Us
=========================================== -->     
<section id="" class="hero-area">
    <div class="container">
        <div class="block">
            <div class="contact-info col-md-6 wow fadeInUp" data-wow-duration="500ms">
                <div class="title text-center wow fadeInUp" >
                <img width="200px" src="<?php echo base_url('img/favicon.png') ?>">
                <h2>Student Advisors</h2>
            </div>
                </div>
            </div>
            <!-- / End Contact Details -->
                
            <!-- Contact Form -->
            <div class="contact-form col-md-6 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">
                <form id="contact-form" method="post" action="<?php echo base_url('Auth/signin') ?>">
                    <?php echo $this->session->flashdata('notify');?>


                    <div class="form-group">
                        <input style="border-color: white;" type="text" placeholder="Username" class="form-control btn-faraday" name="username" id="username">
                    </div>
                    
                    <div class="form-group">
                        <input style="border-color: white;" type="password" placeholder="Password" class="form-control btn-faraday" name="password" id="password">
                    </div>
                    
                    <div id="cf-submit">
                        <input style="background-color: #123053; border-color: grey" type="submit" id="contact-submit" class="btn btn-transparent" value="Log in" name="login">
                    </div>
                    </form>
                    <br>
            <!-- ./End Contact Form -->
        
        </div> <!-- end row -->
    </div> <!-- end container -->



        
    </body>
</html>

<?php 
    
    /*
    85815660500
zj3
https://lp.wifi.id/?NG94RktRQ3drZ05SbEZqOW5yenZ1V245bGhYUE9vQnFuR1pVZXY0eVlnVTllZGNwNE5ZT0pMR2JpbWRRT2VuWi9OL1RlV3d2Q2ZMckJ5akRMSkVRWUlwTG1vb1RiTHBqajBRSFFZTm82Ump6d2k5SDBhUngrWUhvakhZdGh1WTd2cjJPK1I4S3FxUWQwVmM0TDZlODZNM0NORENnVnVyRWF1NGorTG15VHMzOTZMTGp0NTJwZG0wbFd2Ny91UGRq
*/
    
 ?>