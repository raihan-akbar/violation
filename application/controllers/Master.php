<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
// TOUCH THIS U DIE
	function __construct(){
		parent::__construct();
		$this->load->model('Mc');

		if ($this->session->userdata('status') != "login"){
			$this->session->set_flashdata('notify','<b><p style="color: red; text-align: center;">Login Please</p></b><br>');
			redirect(base_url('/'));
			$this->session->sess_destroy();
		}
	}

	public function index(){

		$data['menus'] = $this->Mc->menu()->result();
		$data['menuser'] = $this->Mc->menuUser()->result();
		$data['accounts'] = $this->Mc->getAccount()->result();
		$data['roleAccounts'] = $this->Mc->getRoleAccount()->result();
		$data['vioLastest'] = $this->Mc->vioLast()->result();
		$data['vioCounts'] = $this->Mc->vioCount()->result();
		$data['vioHiPersons'] = $this->Mc->vioHiPerson()->result();
		$data['vioThisMonths'] = $this->Mc->vioThisMonth()->result();
		$data['vioPointPersons'] = $this->Mc->vioPointPerson()->result();

		$this->load->view('home.php', $data);
	}

	public function students(){

		$data['menus'] = $this->Mc->menu()->result();
		$data['menuser'] = $this->Mc->menuUser()->result();
		$data['students'] = $this->Mc->student()->result();
		$data['expertises'] = $this->Mc->expertise()->result();
		$data['accounts'] = $this->Mc->getAccount()->result();
		$data['roleAccounts'] = $this->Mc->getRoleAccount()->result();

		$this->load->view('students.php', $data);
	}

	function offense(){

		$data['menus'] = $this->Mc->menu()->result();
		$data['menuser'] = $this->Mc->menuUser()->result();
		$data['offenses'] = $this->Mc->offense()->result();
		$data['accounts'] = $this->Mc->getAccount()->result();
		$data['roleAccounts'] = $this->Mc->getRoleAccount()->result();
		
		$this->load->view('offense.php', $data);
	}

	public function expertise(){

		$data['menus'] = $this->Mc->menu()->result();
		$data['menuser'] = $this->Mc->menuUser()->result();
		$data['expertises'] = $this->Mc->expertise()->result();
		$data['accounts'] = $this->Mc->getAccount()->result();
		$data['roleAccounts'] = $this->Mc->getRoleAccount()->result();

		$this->load->view('expertise.php', $data);
	}
	public function account(){

		$data['menus'] = $this->Mc->menu()->result();
		$data['menuser'] = $this->Mc->menuUser()->result();
		$data['expertises'] = $this->Mc->expertise()->result();
		$data['accounts'] = $this->Mc->getAccount()->result();
		$data['roleAccounts'] = $this->Mc->getRoleAccount()->result();

		$this->load->view('account.php', $data);
	}

	public function record(){

		$data['menus'] = $this->Mc->menu()->result();
		$data['menuser'] = $this->Mc->menuUser()->result();
		$data['expertises'] = $this->Mc->expertise()->result();
		$data['accounts'] = $this->Mc->getAccount()->result();
		$data['roleAccounts'] = $this->Mc->getRoleAccount()->result();
		$data['recordDatas'] = $this->Mc->getRecordData()->result();

		$this->load->view('record.php', $data);
	}

	public function getStudents(){
		$data['menus'] = $this->Mc->menu()->result();
		$data['menuser'] = $this->Mc->menuUser()->result();
		$data['expertises'] = $this->Mc->expertise()->result();
		$data['accounts'] = $this->Mc->getAccount()->result();
		$data['roleAccounts'] = $this->Mc->getRoleAccount()->result();
		$data['recordDatas'] = $this->Mc->getRecordData()->result();
		$data['cStudent'] = $this->Mc->catchSt()->result();
		$data['vioPointPersons'] = $this->Mc->vioPointPerson()->result();
		$data['students'] = $this->Mc->student()->result();
		$data['offenses'] = $this->Mc->offense()->result();

		$this->load->view('getst.php', $data);
	
	}

	public function user(){
		$data['menus'] = $this->Mc->menu()->result();
		$data['menuser'] = $this->Mc->menuUser()->result();
		$data['accounts'] = $this->Mc->getAccount()->result();
		$data['roleAccounts'] = $this->Mc->getRoleAccount()->result();

		$this->load->view('user.php', $data);

	}

	// BATAS LOAD FILE

	// CATCH

	function catchStudent($id_student){
		//$id_student = $this->input->post('id_student');

		$data_session = array(
				'id_student'  => $id_student
			);

		$this->session->set_userdata($data_session);
		redirect('Master/getStudents/');

	}	

	// Mulai ADD

	function addOffense(){
		$name_offense = $this->input->post('name_offense');
		$level_offense = $this->input->post('level_offense');
		$about_offense = $this->input->post('about_offense');

		$data = array(
			'name_offense' => $name_offense,
			'level_offense' => $level_offense,
			'about_offense' => $about_offense
			);

		$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Saved','Your Data Has Been Successfully Saved', 'success')
    }
 </script>");

		$this->Mc->add($data,'offense');
		redirect('Master/offense/');

		
	}

	function addStudent(){
		$nis = $this->input->post('nis');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$id_expertise = $this->input->post('id_expertise');
		$point = $this->input->post('point');
		$start_period = $this->input->post('start_period');

		$data = array(
			'nis' => $nis,
			'name' => $name,
			'address' => $address,
			'id_expertise' => $id_expertise,
			'point' => $point,
			'start_period' => $start_period
			);

		$this->Mc->add($data,'student');
		redirect('Master/students');

		
	}

	function addExpertise(){
		$name_expertise = $this->input->post('name_expertise');
		$alias_expertise = $this->input->post('alias_expertise');
		$about_expertise = $this->input->post('about_expertise');

		$data = array(
			'name_expertise' => $name_expertise,
			'alias_expertise' => $alias_expertise,
			'about_expertise' => $about_expertise
			);

		$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Saved','Your Data Has Been Successfully Saved', 'success')
    }
 </script>");

		$this->Mc->add($data,'expertise');
		redirect('Master/expertise/');

		
	}
	function addRecord(){
		$id_student = $this->input->post('id_student');
		$id_offense = $this->input->post('id_offense');
		$id_user = $this->input->post('id_user');
		$about_record = $this->input->post('about_record');
		$d_record = $this->input->post('d_record');
		$cost = $this->input->post('cost');

		$date_source = explode('-', $d_record);
		$array = array($date_source[2], $date_source[1], $date_source[0]); 
		$date_record = implode('-', $array);

		$data = array(
			'id_student' => $id_student,
			'id_offense' => $id_offense,
			'id_user' => $id_user,
			'about_record' => $about_record,
			'd_record' => $date_record,
			'cost' => $cost
		);

		$this->Mc->add($data,'record');
		redirect('Master/getStudents/');
	}

	// BATAS ADD

	//Mulai Del

	function delOffense($id_offense){
		$where = array('id_offense'=>$id_offense);

		$this->Mc->del($where,'offense');

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Deleted','Your Data Has Been Successfully Deleted', 'success')
    	}
 		</script>");

		redirect('Master/offense/');
	}

	function delExpertise($id_expertise){
		$where = array('id_expertise'=>$id_expertise);

		$this->session->set_flashdata("notify", "<script>
    	window.onload=function(){
        swal('Deleted','Your Data Has Been Successfully Deleted', 'success')
    	}
 		</script>");

		$this->Mc->del($where,'expertise');
		redirect('Master/expertise/');
	}

	// Akhir Del

	function updOffense(){
		$id_offense = $this->input->post('id_offense');
		$name_offense = $this->input->post('name_offense');
		$level_offense = $this->input->post('level_offense');
		$about_offense = $this->input->post('about_offense');

		$data = array(
			'id_offense' => $id_offense,
			'name_offense' => $name_offense,
			'level_offense' => $level_offense,
			'about_offense' => $about_offense
			);
 
	$where = array(
		'id_offense' => $id_offense
	);

	$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Updated','Your Data Has Been Successfully Updated', 'success')
    }
 </script>");
 
	$this->Mc->upd($where,$data,'offense');
			redirect(base_url("Master/offense/"));
}

function updStudent(){
		$id_student = $this->input->post('id_student');
		$name = $this->input->post('name');
		$nis = $this->input->post('nis');
		$address = $this->input->post('address');
		$id_expertise = $this->input->post('id_expertise');
		$point = $this->input->post('point');
		$start_period = $this->input->post('start_period');

		$data = array(
			'id_student' => $id_student,
			'name' => $name,
			'nis' => $nis,
			'address' => $address,
			'id_expertise' => $id_expertise,
			'point' => $point,
			'start_period' => $start_period
			);
 
	$where = array(
		'id_student' => $id_student
	);
 
	$this->Mc->upd($where,$data,'student');
			redirect(base_url("Master/students/"));
}

function updExpertise(){
		$id_expertise = $this->input->post('id_expertise');
		$name_expertise = $this->input->post('name_expertise');
		$alias_expertise = $this->input->post('alias_expertise');
		$about_expertise = $this->input->post('about_expertise');

		$data = array(
			'id_expertise' => $id_expertise,
			'name_expertise' => $name_expertise,
			'alias_expertise' => $alias_expertise,
			'about_expertise' => $about_expertise
			);
 
	$where = array(
		'id_expertise' => $id_expertise
	);
	
	$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Updated','Your Data Has Been Successfully Updated', 'success')
    }
 </script>");

	$this->Mc->upd($where,$data,'expertise');
			redirect(base_url("Master/expertise/"));
}


function updPassword(){
		$id_user = $this->input->post('id_user');
		$pwd = $this->input->post('pwd');
		$passwd= $this->input->post('passwd');
		$username = $this->input->post('username');
		$user = $this->Mc->check('user',$where)->num_rows();

		$get = array('username' => $username,);

		$options = ['cost' => 12];
		$plive = password_hash($passwd, PASSWORD_BCRYPT, $options);
		$vrap = $this->Mc->check('user',$get)->result();

		

		foreach ($vrap as $p){
		if (password_verify($pwd, $p->password)) {

			$data = array(
			'id_user' => $id_user,
			'password' => $plive
			);

			$where = array(
			'id_user' => $id_user);

			$this->session->set_flashdata('notify','<b><p style="color: white; text-align: center; background-color:green;">Success</p></b><br>');
			$this->Mc->upd($where,$data,'user');
			redirect(base_url("Master/account/"));
		}else{
			$this->session->set_flashdata('notify','<b><p style="color: white; text-align: center; background-color:red;">Incorrect Password</p></b><br>');
			redirect(base_url('/Master/account/'));
		}
 
	
 
}// End Foreach	
}
      

}//End of File


/*
foreach ($roleAccounts as $ra) {
			$ras = $ra->name_role;
		}

		if ($ras != 'Master'){
			$this->session->set_flashdata('rs','
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <i class="mdi mdi-block-helper"></i>
                                                <strong>Sorry</strong> Access Forbidden.
                                            </div>

				');
			redirect(base_url('Master/students/'));
		}
*/


