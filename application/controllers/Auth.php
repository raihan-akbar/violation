<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// Backstage //
class Auth extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Mc');
		$this->load->library('session');
	}

	public function index()
	{
		$this->load->view('signin.php');
	}

	// Please Appreciate Work That Has Been Made By Programmers.
	// Author : Raihan Muhammad Akbar

	function signin(){//0

	$username = $this->input->post('username');
	$password = $this->input->post('password');
		
	$where = array('username' => $username,);
	$user = $this->Mc->check('user',$where)->num_rows();
	
// ADD Another Role Check qui

//Admin Authentication
	if ($user == 1) { //1
		$vrap = $this->Mc->check('user',$where)->result();
		foreach ($vrap as $p){ //2
		if (password_verify($password, $p->password)){ //3

			//$data['accounts'] = $this->Mc->getAccount()->result();

			//Account Session
			$data_session = array(
				'username'  => $username,
				'status' => 'login'//,
				//'as' => 'Master'
			);

			$this->session->set_userdata($data_session);

			$this->session->set_flashdata("notify", "<script>
    window.onload=function(){
        swal('Welcome Back','Glad to see you again !', 'success')
    }
 </script>");

			redirect(base_url('Master/',$data));
			} //>3
		else { //4
			$this->session->set_flashdata('notify','<b><p style="color: white; text-align: center; background-color:red;">Incorrect Password</p></b><br>');
			redirect(base_url('/'));
		} //>4

		} //>2

		} //>1
//Admin Authentication END

//Start Qui if You Want MultiRole Authentication

/*	else if ($helper == 1) { //6
		$vrap = $this->Mc->check('helper',$where)->result();
		foreach ($vrap as $p) { //7
		if (password_verify($password, $p->password)){ //8

			//For Session
			$data_session = array(
				'username'  => $username,
				'status' => 'login',
				'as' => 'Admin'
			);

			redirect(base_url('Master'));
			} //>8

		else { //9
			$this->session->set_flashdata('hits','<b><p style="color: white; text-align: center; background-color:red;">Incorrect Password</p></b><br>');
			redirect(base_url('/'));
		} //>9	

		} //>7 

	} //>6 / End of User 2
*/
	
	else{ //5 / for Nothing Chance Anymore !

		$this->session->set_flashdata('notify','<b><p style="color: white; text-align: center; background-color:red;">Incorrect Username or Password</p></b><br>');
		redirect(base_url('/'));
	} //>5
	
	} //0 / end of -- signin

	function signout(){
			$this->session->sess_destroy();
			redirect(base_url(''));

	}
	
} //END
